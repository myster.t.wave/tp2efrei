# TP2 : Routage, DHCP et DNS


☀️ **Configuration de `router.tp2.efrei`**

```bash
[rocky@routeur1efrei ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=128 time=18.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=128 time=18.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=128 time=18.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=128 time=18.5 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 18.153/18.317/18.492/0.157 ms
[rocky@routeur1efrei ~]$ ping 8.8.4.4
PING 8.8.4.4 (8.8.4.4) 56(84) bytes of data.
64 bytes from 8.8.4.4: icmp_seq=1 ttl=128 time=19.1 ms
64 bytes from 8.8.4.4: icmp_seq=2 ttl=128 time=18.7 ms
64 bytes from 8.8.4.4: icmp_seq=3 ttl=128 time=19.0 ms
64 bytes from 8.8.4.4: icmp_seq=4 ttl=128 time=18.5 ms
64 bytes from 8.8.4.4: icmp_seq=5 ttl=128 time=20.8 ms
^C
--- 8.8.4.4 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 18.477/19.210/20.788/0.818 ms
[rocky@routeur1efrei ~]$
[rocky@routeur1efrei ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e0:05:b0 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fee0:5b0/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d9:19:ec brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.12/24 brd 192.168.56.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fed9:19ec/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:13:c2:d6 brd ff:ff:ff:ff:ff:ff
    inet 192.168.190.131/24 brd 192.168.190.255 scope global dynamic noprefixroute enp0s9
       valid_lft 1333sec preferred_lft 1333sec
    inet6 fe80::7bb2:c87f:ca6b:7c36/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[rocky@routeur1efrei ~]$
```


```bash
[rocky@routeur1efrei ~]$ sudo sysctl -w net.ipv4.ip_forward=1
[sudo] password for rocky:
net.ipv4.ip_forward = 1
[rocky@routeur1efrei ~]$ sudo firewall-cmd --add-masquerade
success
[rocky@routeur1efrei ~]$ sudo firewall-cmd --add-masquerade --permanent
success
```

☀️ **Configuration de `node1.tp2.efrei`**

```bash
[rocky@node1efrei ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:df:7b:f5 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.1/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fedf:7bf5/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:be:c8:95 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.11/24 brd 192.168.56.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:febe:c895/64 scope link
       valid_lft forever preferred_lft forever
[rocky@node1efrei ~]$
```
```bash
[rocky@node1efrei ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.468 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.520 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=0.715 ms
64 bytes from 10.2.1.254: icmp_seq=4 ttl=64 time=0.579 ms
^C
--- 10.2.1.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3108ms
rtt min/avg/max/mdev = 0.468/0.570/0.715/0.092 ms
```
```bash
[rocky@node1efrei ~]$ ping 8.8.4.4
PING 8.8.4.4 (8.8.4.4) 56(84) bytes of data.
64 bytes from 8.8.4.4: icmp_seq=1 ttl=127 time=20.1 ms
64 bytes from 8.8.4.4: icmp_seq=2 ttl=127 time=20.0 ms
^C
--- 8.8.4.4 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 20.022/20.065/20.109/0.043 ms
[rocky@node1efrei ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=127 time=19.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=127 time=19.3 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=127 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=127 time=20.1 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 19.334/19.743/20.204/0.407 ms
```
```bash
[rocky@node1efrei ~]$ sudo traceroute -I -q 1 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  192.168.56.12 (192.168.56.12)  0.747 ms
 2  192.168.190.2 (192.168.190.2)  1.375 ms
 3  *
 4  *
 5  *
 6  *
 7  *
 8  *
 9  dns.google (8.8.8.8)  14.173 ms
```


# II. Serveur DHCP



☀️ **Install et conf du serveur DHCP** sur `dhcp.tp2.efrei`
```bash
[rocky@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:02:82:a5 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.253/24 brd 10.2.1.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe02:82a5/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:61:67:27 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.13/24 brd 192.168.56.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe61:6727/64 scope link
       valid_lft forever preferred_lft forever
[rocky@localhost ~]$
```
```bash
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers     dlp.srv.world;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.220 10.2.1.250;

option domain-name-servers 8.8.8.8;
    # specify gateway
    option routers 10.2.1.254;

[rocky@dhcp1efrei ~]$ ping google.com
PING google.com (192.0.0.88) 56(84) bytes of data.
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=1 ttl=127 time=16.7 ms
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=2 ttl=127 time=13.0 ms
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=3 ttl=127 time=13.5 ms
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=4 ttl=127 time=13.6 ms
^C
--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 13.027/14.212/16.720/1.463 ms
[rocky@dhcp1efrei ~]$
}
```



☀️ **Test du DHCP** sur `node1.tp2.efrei`
```bash
[rocky@node1efrei ~]$ ll /var/lib/dhcpd
ls: cannot access '/var/lib/dhcpd': No such file or directory
[rocky@node1efrei ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0e:d7:33 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.220/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 445sec preferred_lft 445sec
    inet6 fe80::a00:27ff:fe0e:d733/64 scope link
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f7:15:f5 brd ff:ff:ff:ff:ff:ff
    inet 192.168.56.11/24 brd 192.168.56.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fef7:15f5/64 scope link
       valid_lft forever preferred_lft forever
[rocky@node1efrei ~]$

[rocky@node1efrei ~]$ ping google.com
PING google.com (192.0.0.88) 56(84) bytes of data.
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=1 ttl=127 time=14.4 ms
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=2 ttl=127 time=14.3 ms
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=3 ttl=127 time=13.4 ms
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=4 ttl=127 time=13.2 ms
64 bytes from 192.0.0.88 (192.0.0.88): icmp_seq=5 ttl=127 time=13.2 ms
^C
--- google.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 13.157/13.701/14.402/0.551 ms
[rocky@node1efrei ~]$
```


🌟 **BONUS**
```bash
option domain-name-servers 8.8.8.8;
    # specify gateway
    option routers 10.2.1.254;
}
```
☀️ **Wireshark it !**



# III. ARP

## 1. Les tables ARP


☀️ **Affichez la table ARP de `router.tp2.efrei`**
```bash

[rocky@node1efrei ~]$ ip n s dev enp0s3
10.2.1.253 lladdr 08:00:27:ba:9a:5f REACHABLE
10.2.1.254 lladdr 08:00:27:e0:05:b0 REACHABLE
[rocky@node1efrei ~]$
```


## 2. ARP poisoning

☀️ **Exécuter un simple ARP poisoning**

- pas de man in the middle ici ou quoique ce soit, rien d'extrêmement poussé, mais simplement : écrire arbitrairement dans la table ARP de quelqu'un d'autre
- il "suffit" d'envoyer un seul message ARP pour forcer l'écriture dans la table ARP de la machine qui reçoit votre message
- je vous laisse vous renseigner par vous-mêmes un peu pour cette partie !
- le but : écrivez dans la table ARP de `node1` que l'adresse `10.2.1.254` correspond à l'adresse MAC de votre choix
  - **cela a pour conséquence que vous pouvez usurper l'identité de `10.2.1.254` (c'est le routeur) auprès de `node1`**. Stylish.

> C'est faisable super facilement en une seule commande shell : `arping`. Je recommande pas Rocky pour utiliser ça, ce sera chiant de l'installer je pense. Et bien sûr, n'hésitez pas à me contacter.

![APR sniffed ?](img/arp_sniff.jpg)
